package com.cc;

import com.cc.model.WeatherData;
import com.cc.service.CityService;
import com.cc.service.WeatherService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CityServiceTest {

    @InjectMocks
    private CityService cityService;

    @Mock
    private WeatherService weatherService;

    @Test
    public void count() {
        when(weatherService.getResults()).thenReturn(createTestData(
                List.of("London",
                        "Helsinki",
                        "Hong Kong",
                        "Honolulu",
                        "New York")
                )
        );
        assertEquals(1, cityService.countCitiesBeginningWith("L"));
        assertEquals(3, cityService.countCitiesBeginningWith("H"));
    }

    private WeatherData createTestData(List<String> cities) {
        WeatherData weatherData = new WeatherData();
        weatherData.setList(cities.stream()
                .map(WeatherData.CityData::new)
                .collect(Collectors.toList()));
        return weatherData;
    }
}

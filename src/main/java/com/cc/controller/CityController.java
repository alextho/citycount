package com.cc.controller;

import com.cc.service.CityService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.web.bind.annotation.RequestMethod.GET;


@RestController
@RequestMapping("/city")
@Api(tags = "City data")
public class CityController {

    private final CityService cityService;

    @Autowired
    public CityController(CityService cityService) {
        this.cityService = cityService;
    }

    @ApiOperation(value = "Count cities beginning with prefix")
    @RequestMapping(value = "/count/{prefix}", method = GET)
    public ResponseEntity<Long> countCities(@PathVariable String prefix) {
        return ResponseEntity.ok(cityService.countCitiesBeginningWith(prefix));
    }
}
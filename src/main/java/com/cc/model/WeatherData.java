package com.cc.model;

import java.util.List;

public class WeatherData {

    public static class CityData {
        private String name;

        public CityData() {
        }

        public CityData(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    private List<CityData> list;

    public List<CityData> getList() {
        return list;
    }

    public void setList(List<CityData> list) {
        this.list = list;
    }
}

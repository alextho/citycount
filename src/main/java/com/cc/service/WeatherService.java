package com.cc.service;

import com.cc.model.WeatherData;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;

@Service
public class WeatherService {

    private final String api;
    private final ObjectMapper objectMapper;

    @Autowired
    public WeatherService(@Value("${app.weather.api}") String api,
                          ObjectMapper objectMapper) {
        this.api = api;
        this.objectMapper = objectMapper;
    }

    /**
     * Call the Weather API and return a the weather data for the specific pre-defined location.
     * @return Weather data results.
     */
    public WeatherData getResults() {
        HttpGet request = new HttpGet(api);

        WeatherData weatherData;

        try (CloseableHttpClient httpClient = HttpClientBuilder.create().build()) {
            try (CloseableHttpResponse response = httpClient.execute(request)) {
                if (response.getStatusLine().getStatusCode() >= 400) {
                    throw new RuntimeException("Request failed [" + EntityUtils.toString(response.getEntity()) + "]");
                }
                weatherData = objectMapper.readValue(response.getEntity().getContent(), new TypeReference<WeatherData>() {
                });
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return weatherData;
    }
}

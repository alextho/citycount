package com.cc.service;

import com.cc.model.WeatherData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class CityService {

    private final WeatherService weatherService;

    @Autowired
    public CityService(WeatherService weatherService) {
        this.weatherService = weatherService;
    }

    /**
     * Call the Weather api and count the cities beginning with the given prefix.
     * @param prefix String prefix used to filter city names.
     * @return A long representing the number of cities matching the given prefix.
     */
    public long countCitiesBeginningWith(String prefix) {
        final String prefixLowerCase = prefix.toLowerCase();
        WeatherData weatherData = weatherService.getResults();
        List<String> cities = getCitiesFromWeatherData(weatherData);
        return cities.stream()
                .filter(c -> c != null && c.toLowerCase().startsWith(prefixLowerCase))
                .count();
    }

    private List<String> getCitiesFromWeatherData(WeatherData weatherData) {
        return weatherData.getList().stream()
                .map(WeatherData.CityData::getName)
                .collect(Collectors.toList());
    }
}
